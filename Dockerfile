FROM nginx:1.25.4-alpine

RUN adduser -D erick
#USER erick

COPY ./index.html /usr/share/nginx/html

HEALTHCHECK --timeout=10s CMD wget -O /dev/null http://localhost || exit 1